import { Component, OnInit } from '@angular/core';
import { EntryEgressService } from '../entryEgress/entry-egress.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  constructor(private entryEgress: EntryEgressService) {}

  ngOnInit(): void {
    this.entryEgress.initEntryEgressListener();
  }
}
