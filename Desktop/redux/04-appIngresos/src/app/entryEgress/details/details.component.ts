import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppState } from '../../app.reducer';
import { Store } from '@ngrx/store';
import { EntryEgressModel } from 'src/app/models/entry-egress.model';
import { Subscription } from 'rxjs';
import { EntryEgressService } from '../entry-egress.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit, OnDestroy {
  suscription: Subscription = new Subscription();
  items: EntryEgressModel[];
  constructor(
    private store: Store<AppState>,
    private EntryEgressService: EntryEgressService
  ) {}

  ngOnInit(): void {
    this.suscription = this.store.select('entryEgress').subscribe((data) => {
      this.items = data.items;
    });
  }
  ngOnDestroy() {
    this.suscription.unsubscribe();
  }
  deleteItem(uid) {
    this.EntryEgressService.delete(uid);
  }
}
