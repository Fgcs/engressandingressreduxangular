import { Action } from '@ngrx/store';
import { EntryEgressModel } from '../models/entry-egress.model';
export const SET_ITEMS = '[Entry-Egress] set items';
export const UNSET_ITEMS = '[Entry-Egress] unset items';

export class setItemsActions implements Action {
  readonly type = SET_ITEMS;
  constructor(public items: EntryEgressModel[]) {}
}
export class unSetItemsactions implements Action {
  readonly type = UNSET_ITEMS;
}
export type accions = setItemsActions | unSetItemsactions;
