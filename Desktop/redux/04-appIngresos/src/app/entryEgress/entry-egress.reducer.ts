import { EntryEgressModel } from '../models/entry-egress.model';
import * as fromEntryEgress from './entry-egress.actions';

export interface EntryEgressState {
  items: EntryEgressModel[];
}

const initState: EntryEgressState = {
  items: [],
};
export function EntryEgressReducer(
  state = initState,
  action: fromEntryEgress.accions
): EntryEgressState {
  switch (action.type) {
    case fromEntryEgress.SET_ITEMS:
      return {
        items: [
          ...action.items.map((item) => {
            return {
              ...item,
            };
          }),
        ],
      };
    case fromEntryEgress.UNSET_ITEMS:
      return {
        items: [],
      };
    default:
      return state;
  }
}
