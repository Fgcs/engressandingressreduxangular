import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { EntryEgressModel } from '../models/entry-egress.model';
import { AuthService } from '../auth/auth.service';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { filter, map } from 'rxjs/operators';
import { setItemsActions } from './entry-egress.actions';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class EntryEgressService {
  entryegressListenerSuscription: Subscription = new Subscription();
  entryegressItemsSuscription: Subscription = new Subscription();

  constructor(
    private afDB: AngularFirestore,
    private auth: AuthService,
    private store: Store<AppState>
  ) {}

  initEntryEgressListener() {
    this.entryegressListenerSuscription = this.store
      .select('auth')
      .pipe(filter((auth) => auth.user != null))
      .subscribe((auth) => this.entryEgressItems(auth.user.uid));
  }
  entryEgressItems(uid: string) {
    this.entryegressItemsSuscription = this.afDB
      .collection(`${uid}/entry-egress/items`)
      .snapshotChanges()
      .pipe(
        map((docData) => {
          return docData.map((item) => {
            return {
              uid: item.payload.doc.id,
              ...(item.payload.doc.data() as {}),
            };
          });
        })
      )
      .subscribe((data: any) => {
        this.store.dispatch(new setItemsActions(data));
      });
  }
  suscriptionsCancel() {
    this.entryegressItemsSuscription.unsubscribe();
    this.entryegressListenerSuscription.unsubscribe();
  }
  create(body: EntryEgressModel) {
    const user = this.auth.getUser();
    return this.afDB
      .doc(`${user.uid}/entry-egress`)
      .collection('items')
      .add({
        ...body,
      });
  }
  delete(uid: string) {
    const user = this.auth.getUser();
    return this.afDB
      .doc(`${user.uid}/entry-egress/items/${uid}`)
      .delete()
      .then(() => {
        Swal.fire({
          title: 'success!',
          text: 'Eliminando correctamente',
          icon: 'success',
          confirmButtonText: 'Cool',
        });
      });
  }
}
