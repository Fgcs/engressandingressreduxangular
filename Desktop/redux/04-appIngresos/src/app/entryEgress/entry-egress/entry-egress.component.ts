import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EntryEgressModel } from '../../models/entry-egress.model';
import { EntryEgressService } from '../entry-egress.service';

import Swal from 'sweetalert2';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import { Subscription } from 'rxjs';
import {
  ActiveLoadingAction,
  InactiveLoadingAction,
} from '../../shared/ui.accions';
@Component({
  selector: 'app-entry-egress',
  templateUrl: './entry-egress.component.html',
  styleUrls: ['./entry-egress.component.css'],
})
export class EntryEgressComponent implements OnInit, OnDestroy {
  form: FormGroup;
  tipo = 'entry';
  loadingSubs: Subscription = new Subscription();
  loading: boolean;
  constructor(
    private fb: FormBuilder,
    private entryEgresService: EntryEgressService,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.loadingSubs = this.store
      .select('ui')
      .subscribe((ui) => (this.loading = ui.isLoading));
    this.form = this.fb.group({
      description: ['', Validators.required],
      monto: [0, Validators.min(0)],
    });
  }
  ngOnDestroy() {
    this.loadingSubs.unsubscribe();
  }
  create() {
    this.store.dispatch(new ActiveLoadingAction());
    const body = new EntryEgressModel({ ...this.form.value, type: this.tipo });
    this.entryEgresService
      .create(body)
      .then(() => {
        Swal.fire({
          title: 'success!',
          text: 'creado',
          icon: 'success',
          confirmButtonText: 'Cool',
        });
        this.form.reset({
          monto: 0,
        });
        this.store.dispatch(new InactiveLoadingAction());
      })
      .catch((err) => {
        this.store.dispatch(new InactiveLoadingAction());
        Swal.fire({
          title: 'Error!',
          text: err.message,
          icon: 'error',
          confirmButtonText: 'Cool',
        });
      });
  }
}
