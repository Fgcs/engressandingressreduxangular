import { Pipe, PipeTransform } from '@angular/core';
import { EntryEgressModel } from '../models/entry-egress.model';

@Pipe({
  name: 'orderEntryEgress',
})
export class OrderEntryEgressPipe implements PipeTransform {
  transform(items: EntryEgressModel[]): EntryEgressModel[] {
    return items.sort((a, b) => {
      if (a.type === 'entry') {
        return -1;
      } else {
        return 1;
      }
    });
  }
}
