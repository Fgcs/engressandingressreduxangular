import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import { Subscription } from 'rxjs';
import { EntryEgressModel } from '../../models/entry-egress.model';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
})
export class StatisticsComponent implements OnInit {
  totalEntry: number;
  totalEgress: number;
  Entry: number;
  Egress: number;
  suscription: Subscription = new Subscription();
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.suscription = this.store
      .select('entryEgress')
      .subscribe((data) => this.countEntryEgress(data.items));
  }
  countEntryEgress(items: EntryEgressModel[]) {
    this.totalEgress = 0;
    this.totalEntry = 0;
    this.Egress = 0;
    this.Entry = 0;
    items.forEach((item) => {
      if (item.type === 'entry') {
        this.totalEntry++;
        this.Entry += item.monto;
      } else {
        this.Egress += item.monto;
        this.totalEgress++;
      }
    });
  }
}
