export class EntryEgressModel {
  description: string;
  type: string;
  uid?: string;
  monto: number;
  constructor(obj: any) {
    this.description = (obj && obj.description) || null;
    this.type = (obj && obj.type) || null;
    // this.uid = (obj && obj.uid) || null;
    this.monto = (obj && obj.monto) || null;
  }
}
